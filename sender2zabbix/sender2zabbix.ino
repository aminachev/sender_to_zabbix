//*****************************************************************************************
//* Purpose : Zabbix Sender *
//* Author : Schotte Vincent *
//
// 
//  we are going to send a counter to zabbix 
//
//  create a hostname : arduinocounter
//           item/key : counter                 Type : Zabbix trapper
//                                              Type of Information : Numeric 
//
// скетч отправляет каждые две минуты данные на заббикс-сервер о потреблении электроэнергии за минуту
//-----------------INCLUDES--------------------
#include <SPI.h>
#include <Ethernet.h>
#include <Base64.h>


//Network staff
//--------------------------------------------
byte mac[] = { 0xDE, 0xAA, 0xBB, 0xEF, 0xFE, 0x92 };

IPAddress ip(192, 168, 0, 155);           // ip address arduino
IPAddress gateway(192, 168, 0, 1);
IPAddress subnet(255, 255, 255, 0);

IPAddress zabbix(192,168,0,106);  // you're zabbix server

char host[] = "powermeter";     // hostname zabbix
char base64host[200];

char key1[] = "wh";            // item  zabbix 
char base64key1[200];

char value1[10];                    // value of counter
char base64value1[200];

int counter;


EthernetClient zabclient ;

//Необходимое для работы счетчика энергии
//Number of pulses, used to measure energy.
long pulseCount = 0;

//Used to measure power.
unsigned long pulseTime,lastTime;

//power and energy
double power ;
int elapsedWh=0,old_elapsedWh=0;
//Number of pulses per wh - found or set on the meter.
double ppwh = 4; //1000 pulses/kwh = 1 pulse per wh
//3200 pulses/kWh=3.2
//4000 pulses/kWh=4 pulse per wh

const long interval = 60000; 
unsigned long previousMillis = 0;

void setup()
{

    counter = 1 ;                                    // init the counter
    Ethernet.begin(mac, ip, gateway, subnet);         
    Serial.begin(115200);
     pinMode(3, INPUT_PULLUP);
      pinMode(13,OUTPUT);
    // KWH interrupt attached to IRQ 1 = pin3
    attachInterrupt(1, onPulse, FALLING);
}

void loop()

{
   unsigned long currentMillis = millis();

   if (currentMillis - previousMillis >= interval) 
        {
    
          sendzabbix(); 
          elapsedWh=0;
          pulseCount=0;
          previousMillis=currentMillis;
        }


}
// The interrupt routine
void onPulse()
{
    //used to measure time between pulses.
    lastTime = pulseTime;
    pulseTime = micros();

    //pulseCounter
    pulseCount++;

    //Calculate power
   // power = (3600000000.0 / (pulseTime - lastTime))/ppwh;

    //Find  kwh elapsed
   
    elapsedWh = pulseCount/ppwh; //multiply by 1000 to convert pulses per wh to kwh

    //Print the values.
    Serial.print("Wh:");
    Serial.println(elapsedWh);

    digitalWrite(13, HIGH);
    digitalWrite(13, LOW);
    
    Serial.print("Pulse:");
    Serial.println(pulseCount);
}

//--------------------------------------------

void sendzabbix()
{
 if (zabclient.connect(zabbix,10051))
  {
   base64_encode(base64host , host , sizeof(host)-1);
   base64_encode(base64key1 , key1 , sizeof(key1)-1);
   
   itoa(elapsedWh,value1,10);       // transform int to char
   
   base64_encode(base64value1 , value1 , sizeof(value1)-1);

   Serial.print("Connected with zabbix, sending info: ");
   Serial.println(elapsedWh);
   
   zabclient.write("<req>\n");
   zabclient.write("  <host>");
   zabclient.write(base64host);
   zabclient.write("</host>\n");
   zabclient.write("  <key>");
   zabclient.print(base64key1);
   zabclient.write("</key>\n");
   zabclient.write("  <data>");
   zabclient.write(base64value1);
   zabclient.write("</data>\n");
   zabclient.write("</req>\n");
   
   delay(1);
   zabclient.stop();
   }
}

// end of code  , have fun
